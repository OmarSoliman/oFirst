<?php

namespace OFirst;

class Calculator
{

    // add two variables
    public function add($x, $y)
    {
        return $x + $y ;
    }

    // subtract two variables
    public function subtract($x, $y)
    {
        return $x - $y ;
    }

    // multiply two variables
    public function multiply($x, $y)
    {
        return $x * $y ;
    }

    // divide two variables
    public function divide($x, $y)
    {
        return $x / $y ;
    }

    // mod two variables
    public function mod($x, $y)
    {
        return $x % $y ;
    }
}